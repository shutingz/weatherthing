import Foundation

/*********************** BASE URLS AND FORMATION FUNCTION *************************/
let baseUrlFiveDays = URL(string: "https://openweathermap.org/data/2.5/forecast?")!;
let baseUrlToday = URL(string: "https://openweathermap.org/data/2.5/weather?")!;
extension URL {
  func withQueries(_ queries: [String: String]) -> URL? {
    var components = URLComponents(url: self, resolvingAgainstBaseURL: true);
    components?.queryItems = queries.compactMap {
      URLQueryItem(name: $0.0, value:$0.1);
    };
    return components?.url;
  }
}

/****************************** LOAD CITY JSON FILE *******************************/
let main_path = URL(fileURLWithPath: #file)
let parent_dir = main_path.deletingLastPathComponent().deletingLastPathComponent()
let data_dir = parent_dir.appendingPathComponent("data/json/")
let file = data_dir.appendingPathComponent("city_list_min.json")
let decoder = JSONDecoder()


/******************************* GATHER LIST OF CITY *******************************/
print("Saisissez un nom de ville, ou plusieurs séparées par ',' sans espace : ")
// exemple: Tokyo,Dijon
var responseCity: String = ""
if let answer = readLine() {
    responseCity = responseCity + answer
}
let cityArray : Array<Substring> = responseCity.split(separator: ",")
var responseID: String = ""
var responseIDList: Array<Substring> = [""]


/********************** GATHER LIST OF ID WITH AUTOCOMPLETION **********************/
do {
    let contents = try String(contentsOf: file)
    let jsonData = contents.data(using: .utf8)!
    let dataStruct = try! decoder.decode(DataStruct.self, from: jsonData)
    for dataStructElement in dataStruct{
        let currentCountry = dataStructElement.name
        for city in cityArray{
            if currentCountry.contains(city){
                print("city : " + dataStructElement.name + ", country : " + dataStructElement.country + ", id: " + String(dataStructElement.id))
            }
        }
    }
    print("Choisissez une ville en tapant son id, ou plusieurs séparées par ',' sans espace : ")
    // exemple for Tokyo and Dijon: 1850144,6453767
    if let answerID = readLine() {
        responseID = responseID + answerID
    }
    responseIDList = responseID.split(separator: ",")
}catch {
    print("Error encoutered. Content could not be loaded.")
}


/******************************* CURRENT FORECAST *********************************/
for ID in responseIDList{
    let query: [String: String] = [
        "id": String(ID),
        "appid": "439d4b804bc8187953eb36d2a8c26a02"
    ];
    var lock: Bool = true
    let url: URL = baseUrlToday.withQueries(query)!;
    let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
        guard let data = data else { return }
        let serverResponse = (String(data: data, encoding: .utf8)!)        
        let contents: String = serverResponse as String
        let jsonData = contents.data(using: .utf8)!
        do {
            let weatherReport = try decoder.decode(WeatherReport.self, from: jsonData)
            print("***  CURRENT WEATHER  ***")
            print("City:           " , weatherReport.name)
            print("Country:        ", weatherReport.sys.country)
            print("Weather:        " , weatherReport.weather[0].main, ": ", weatherReport.weather[0].weatherDescription)
            print("humidity        " , weatherReport.main.humidity)
            print("temp feels like " , weatherReport.main.feelsLike)
            print("temp min        " , weatherReport.main.tempMin)
            print("temp max        " , weatherReport.main.tempMax)
            print("temp            " , weatherReport.main.temp)
            print("pressure        " , weatherReport.main.pressure)
            print("wind speed      " , weatherReport.wind.speed)
            print("wind degree     " , weatherReport.wind.deg, "°\n")
        }catch{
            print("Error encoutered. Please check: 1)if the ids are correct 2)if network is fine 3)if you make too many calls to the API.\n")
        }
        lock = false;
    }
    task.resume()
    while lock == true{};
}


/******************************* 5 DAYS FORECAST **********************************/
for ID in responseIDList{
    let query: [String: String] = [
        "id": String(ID),
        "appid": "439d4b804bc8187953eb36d2a8c26a02"
    ];
    var lock: Bool = true
    let url = baseUrlFiveDays.withQueries(query)!;
    let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
        guard let data = data else { return }
        let serverResponseFiveDays = (String(data: data, encoding: .utf8)!)        
        let contents: String = serverResponseFiveDays as String
        let jsonData = contents.data(using: .utf8)!
        do {
            let weatherReport = try decoder.decode(FiveDayWeather.self, from: jsonData)
            print("City:           " , weatherReport.city.name, "***  5 DAYS WEATHER  ***")
            for days in weatherReport.list{
                print("DATE:", days.dtTxt,
                      "|weather:", days.weather[0].weatherDescription,
                      "|humidity:" , days.main.humidity,
                      "|temp min:" , days.main.tempMin,
                      "|temp max:" , days.main.tempMax,
                      "|temp:" , days.main.temp,
                      "|pressure:" , days.main.pressure,
                      "|wind speed:" , days.wind.speed,
                      "|wind degree:" , days.wind.deg, "°")
            }
            print("Fin affichage de la ville: ", weatherReport.city.name)
            print("======================================================")

        }catch{
            print("Error encoutered. Please check: 1)if the ids are correct 2)if network is fine 3)if you make too many calls to the API 4)if the struct weatherReport is missing something \n")            
        }
        lock = false
    }
    task.resume()
    while lock == true{};
}
RunLoop.main.run()

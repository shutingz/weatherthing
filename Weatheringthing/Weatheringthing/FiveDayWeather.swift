//   let fiveDayWeather = try? newJSONDecoder().decode(FiveDayWeather.self, from: jsonData)

import Foundation

// MARK: - FiveDayWeather
struct FiveDayWeather: Codable {
    let message: Int
    let cod: String
    let cnt: Int
    let list: [List]
    let city: City
}

// MARK: - City
struct City: Codable {
    let sunset: Int
    let country: String
    let id: Int
    let coord: FiveDCoordinates
    let sunrise, timezone: Int
    let name: String
}

// MARK: - Coord
struct FiveDCoordinates: Codable {
    let lat, lon: Double
}

// MARK: - List
struct List: Codable {
    let clouds: CloudsData
    let wind: WindData
    let dt: Int
    let rain: Rain?
    let dtTxt: String
    let main: MainClass
    let weather: [WeatherData]
    let sys: SysData

    enum CodingKeys: String, CodingKey {
        case clouds, wind, dt, rain
        case dtTxt = "dt_txt"
        case main, weather, sys
    }
}

// MARK: - Clouds
struct CloudsData: Codable {
    let all: Int
}

// MARK: - MainClass
struct MainClass: Codable {
    let humidity: Int
    let feelsLike, tempMin, tempMax, temp: Double
    let pressure: Int
    let tempKf: Double
    let seaLevel, grndLevel: Int

    enum CodingKeys: String, CodingKey {
        case humidity
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case temp, pressure
        case tempKf = "temp_kf"
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
    }
}

// MARK: - Rain
struct Rain: Codable {
    let the3H: Double

    enum CodingKeys: String, CodingKey {
        case the3H = "3h"
    }
}

// MARK: - Sys
struct SysData: Codable {
    let pod: Pod
}

enum Pod: String, Codable {
    case d = "d"
    case n = "n"
}

// MARK: - Weather
struct WeatherData: Codable {
    let id: Int
    let main: MainEnum
    let icon: String
    let weatherDescription: Description

    enum CodingKeys: String, CodingKey {
        case id, main, icon
        case weatherDescription = "description"
    }
}

enum MainEnum: String, Codable {
    case clear = "Clear"
    case clouds = "Clouds"
    case rain = "Rain"

}

enum Description: String, Codable {
    case brokenClouds = "broken clouds"
    case clearSky = "clear sky"
    case fewClouds = "few clouds"
    case lightRain = "light rain"
    case moderateRain = "moderate rain"
    case overcastClouds = "overcast clouds"
    case scatteredClouds = "scattered clouds"
}

// MARK: - Wind
struct WindData: Codable {
    let speed: Double
    let deg: Int
}

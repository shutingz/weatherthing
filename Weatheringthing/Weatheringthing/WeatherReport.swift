//   let weatherReport = try? newJSONDecoder().decode(WeatherReport.self, from: jsonData)

import Foundation

// MARK: - WeatherReport
struct WeatherReport: Codable {
    let base: String
    let id, dt: Int
    let main: Main
    let coord: Coordinates
    let wind: Wind
    let sys: Sys
    let weather: [Weather]
    let clouds: Clouds
    let timezone, cod: Int
    let name: String
}

// MARK: - Clouds
struct Clouds: Codable {
    let all: Int
}

// MARK: - Coord
struct Coordinates: Codable {
    let lon, lat: Double
}

// MARK: - Main
struct Main: Codable {
    let humidity: Int
    let feelsLike, tempMin, tempMax, temp: Double
    let pressure: Int

    enum CodingKeys: String, CodingKey {
        case humidity
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case temp, pressure
    }
}

// MARK: - Sys
struct Sys: Codable {
    let country: String
    let sunrise, sunset: Int
}

// MARK: - Weather
struct Weather: Codable {
    let id: Int
    let main, icon, weatherDescription: String

    enum CodingKeys: String, CodingKey {
        case id, main, icon
        case weatherDescription = "description"
    }
}

// MARK: - Wind
struct Wind: Codable {
    let speed: Double
    let deg: Int
}

//   let dataStruct = try? newJSONDecoder().decode(DataStruct.self, from: jsonData)

import Foundation

// MARK: - DataStructElement
struct DataStructElement: Codable {
    let id: Int
    let name, state, country: String
    let coord: Coord
}

// MARK: - Coord
struct Coord: Codable {
    let lon, lat: Double
}

typealias DataStruct = [DataStructElement]
